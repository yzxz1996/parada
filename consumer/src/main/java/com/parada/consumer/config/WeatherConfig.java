package com.parada.consumer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * create by cgg 2020/07/13
 */
@Data
@Component
@ConfigurationProperties(value = "weather")
public class WeatherConfig {
    private String wUrl;
    private String appid;
    private String appsecret;
    private String version;


    public String getWeatherUrl(String city) {
        StringBuffer sb = new StringBuffer(getWUrl());
        sb.append("version=").append(getVersion());
        sb.append("&appid=").append(getAppid());
        sb.append("&appsecret=").append(getAppsecret());
        sb.append("&city=").append(city);
        return sb.toString();
    }
}
