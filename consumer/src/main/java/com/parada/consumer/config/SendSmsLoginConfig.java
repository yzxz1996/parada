package com.parada.consumer.config;

import com.parada.common.utils.RedisUtils;
import com.parada.common.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.RedisPipeline;

import static com.parada.common.cons.SupportEntity.LOGIN_PHONE_SMSCODE;
import static com.parada.common.cons.SupportEntity.REGISTER_PHONE_SMSCODE;

/**
 * create by Leo 2020/07/13
 */
@Component
public class SendSmsLoginConfig {
    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private SmsConfig smsConfig;


    public String signUrl(String phoneNumber) throws Exception {

        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(new java.util.SimpleTimeZone(0, "GMT"));// 这里一定要设置GMT时区
        java.util.Map<String, String> paras = new java.util.HashMap<String, String>();
        // 1. 系统参数
        paras.put("SignatureMethod", smsConfig.getSignatureMethod());
        paras.put("SignatureNonce", UUIDUtils.getUUIDStr());
        paras.put("AccessKeyId", smsConfig.getAccessKeyId());
        paras.put("SignatureVersion", smsConfig.getSignatureVersion());
        paras.put("Timestamp", df.format(new java.util.Date()));
        paras.put("Format", smsConfig.getFormat());
        // 2. 业务API参数
        paras.put("Action", smsConfig.getAction());
        paras.put("Version", smsConfig.getVersion());
        paras.put("PhoneNumbers", phoneNumber);
        paras.put("SignName", "parada商城");
        Integer code = (int) (Math.random() * 1000000);
        redisUtils.set(LOGIN_PHONE_SMSCODE + phoneNumber, code.toString(), 300);
        paras.put("TemplateParam", "{\"code\":" + code + "}");
        paras.put("TemplateCode", smsConfig.getTemplateCode());
        // 3. 去除签名关键字Key
        if (paras.containsKey("Signature"))
            paras.remove("Signature");
        // 4. 参数KEY排序
        java.util.TreeMap<String, String> sortParas = new java.util.TreeMap<String, String>();
        sortParas.putAll(paras);
        // 5. 构造待签名的字符串
        java.util.Iterator<String> it = sortParas.keySet().iterator();
        StringBuilder sortQueryStringTmp = new StringBuilder();
        while (it.hasNext()) {
            String key = it.next();
            sortQueryStringTmp.append("&").append(specialUrlEncode(key)).append("=").append(specialUrlEncode(paras.get(key)));
        }
        String sortedQueryString = sortQueryStringTmp.substring(1);// 去除第一个多余的&符号
        StringBuilder stringToSign = new StringBuilder();
        stringToSign.append("GET").append("&");
        stringToSign.append(specialUrlEncode("/")).append("&");
        stringToSign.append(specialUrlEncode(sortedQueryString));
        String sign = sign(smsConfig.getAccessSecret() + "&", stringToSign.toString());
        // 6. 签名最后也要做特殊URL编码
        String signature = specialUrlEncode(sign);
        // 最终打印出合法GET请求的URL
        return "https://dysmsapi.aliyuncs.com/?Signature=" + signature + sortQueryStringTmp;
    }

    public static String specialUrlEncode(String value) throws Exception {
        return java.net.URLEncoder.encode(value, "UTF-8").replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
    }

    public static String sign(String accessSecret, String stringToSign) throws Exception {
        javax.crypto.Mac mac = javax.crypto.Mac.getInstance("HmacSHA1");
        mac.init(new javax.crypto.spec.SecretKeySpec(accessSecret.getBytes("UTF-8"), "HmacSHA1"));
        byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
        return new sun.misc.BASE64Encoder().encode(signData);
    }
}
