package com.parada.consumer.config;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * create by Leo 2020/07/14
 */
@Data
@Component
@ConfigurationProperties(prefix = "sms")
public class SmsConfig {
    private String signatureMethod;
    private String accessKeyId;
    private String accessSecret;
    private String signatureVersion;
    private String format;
    private String action;
    private String version;
    private String signName;
    private String templateCode;
}
