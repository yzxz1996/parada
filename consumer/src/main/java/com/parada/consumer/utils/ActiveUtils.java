package com.parada.consumer.utils;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Destination;

/**
 * Created by xueqijun on 2020/7/8.
 */
@Component
public class ActiveUtils {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    //发送点对点消息
    public void sendQueueMsg(String name, Object msg) {
        Destination destination = new ActiveMQQueue(name);
        jmsMessagingTemplate.convertAndSend(destination, msg);
    }

    //订阅消息
    public void sendTopicMsg(String name, Object msg) {
        Destination destination = new ActiveMQTopic(name);
        jmsMessagingTemplate.convertAndSend(destination, msg);
    }
}
