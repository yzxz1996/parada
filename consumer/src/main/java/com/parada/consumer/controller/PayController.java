package com.parada.consumer.controller;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Maps;
import com.parada.common.config.interAPI.CurrentUser;
import com.parada.common.config.interAPI.LoginRequired;
import com.parada.common.param.ClothesOrderParam;
import com.parada.common.service.ClothesOrderService;
import com.parada.common.service.ClothesService;
import com.parada.common.service.PayService;
import com.parada.common.utils.*;
import com.parada.common.vo.ClothesOrderVo;
import com.parada.common.vo.UsersVo;
import com.parada.consumer.config.WxPayConfig;
import com.parada.consumer.utils.ActiveUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import static com.parada.common.cons.SupportEntity.*;

/**
 * create by Leo 2020/07/16
 */
@Api(tags = "支付相关")
@RestController
@RequestMapping(value = "/pay")
public class PayController {

    @Reference
    private PayService payService;

    @Autowired
    private ActiveUtils activeUtils;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private WxPayConfig wxPayConfig;

    @Reference
    private ClothesOrderService clothesOrderService;

    @Reference
    private ClothesService clothesService;

    @ApiOperation(value = "立即购买")
    @LoginRequired
    @GetMapping(value = "/payImmediately")
    public ReturnResult payImmediately(@CurrentUser UsersVo usersVo, @Validated ClothesOrderParam clothesOrderParam) throws Exception {
        ClothesOrderVo clothesOrderVo = new ClothesOrderVo();
        clothesOrderVo.setId(clothesOrderParam.getClothesId().split("_")[0] + clothesOrderParam.getClothesId().split("_")[1] + "-" + UUIDUtils.getUUIDStrByOwnLen(6) + usersVo.getUserId());
        clothesOrderVo.setUserId(usersVo.getUserId());
        clothesOrderVo.setClothesId(clothesOrderParam.getClothesId());
        clothesOrderVo.setNum(clothesOrderParam.getNum());
        clothesOrderVo.setDisId(clothesOrderParam.getDisId());
        String payStr = payService.payOrder(clothesOrderVo);
        activeUtils.sendQueueMsg(SEND_QUEUE_ORDER_NAME, clothesOrderVo);
        if (StringUtils.isEmpty(payStr)) {
            return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, PAY_UNKNOWN_ERROR);
        }
        return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, payStr);
    }

    @JmsListener(destination = SEND_QUEUE_ORDER_NAME)
    public void addOrder(ClothesOrderVo clothesOrderVo) {
        boolean flag = clothesOrderService.checkNum(clothesOrderVo);
        if (flag) {
            clothesOrderService.insertOrder(clothesOrderVo);
        }
    }

    @ApiIgnore
    @RequestMapping(value = "/wxPayResult")
    public String wxPayResult(HttpServletRequest request) throws Exception {
        InputStream inputStream = request.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuffer sb = new StringBuffer();
        String line;
        while (null != (line = bufferedReader.readLine())) {
            sb.append(line);
        }
        bufferedReader.close();
        inputStream.close();
        Map<String, String> resultMap = WxPayUtils.xmlToMap(sb.toString());
        String orderId = resultMap.get("out_trade_no");
        if (redisUtils.get(WX_PAY_NOTIFY_NAMESPACE + orderId) == null) {
            //签名校验
            if (WxPayUtils.checkSign(resultMap, wxPayConfig.getKey())) {
                //todo 如果支付成功 减库存 修改订单状态
                clothesOrderService.cutCountAndUpdateOrders(orderId);
                activeUtils.sendQueueMsg(CLOTHES_SALE_SUCCESS, orderId);
                redisUtils.set(WX_PAY_NOTIFY_NAMESPACE + orderId, new String[0]);
                Map<String, String> wxMap = Maps.newHashMap();
                wxMap.put("return_code", "SUCCESS");
                wxMap.put("result_msg", "OK");
                return WxPayUtils.mapToXml(wxMap);
            }
        }
        return null;
    }

    @JmsListener(destination = CLOTHES_SALE_SUCCESS)
    public void addSale(String orderId) {
        clothesService.addSale(orderId);
    }
}
