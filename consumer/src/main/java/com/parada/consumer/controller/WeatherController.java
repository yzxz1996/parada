package com.parada.consumer.controller;

import com.parada.common.utils.HttpClientUtils;
import com.parada.consumer.config.WeatherConfig;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * create by cgg 2020/07/13
 */
@RestController
@RequestMapping(value = "/WeatherController")
@Api(tags = "对接天气")
public class WeatherController {

    @Autowired
    private WeatherConfig weatherConfig;

    @GetMapping(value = "/weather")
    public String weatherSearch(@RequestParam String city) throws IOException {
        String weatherMsg = HttpClientUtils.doGet(weatherConfig.getWeatherUrl(city));
        return weatherMsg;
    }
}
