package com.parada.consumer.controller;


import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.parada.common.config.interAPI.CurrentUser;
import com.parada.common.config.interAPI.LoginRequired;
import com.parada.common.param.UserParam;
import com.parada.common.service.UserService;
import com.parada.common.utils.HttpClientUtils;
import com.parada.common.utils.RedisUtils;
import com.parada.common.utils.ReturnResult;
import com.parada.common.utils.ReturnResultUtils;
import com.parada.common.vo.UserInfoVo;
import com.parada.common.vo.UsersVo;
import com.parada.consumer.config.SendSmsLoginConfig;
import com.parada.consumer.config.SendSmsReigisterConfig;
import com.parada.consumer.config.WxConfig.WxLoginConfig;
import com.sun.org.apache.xml.internal.dtm.ref.sax2dtm.SAX2DTM2;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.parada.common.cons.SupportEntity.*;

/**
 * create by Leo 2020/07/13
 */
@Api(tags = "用户注册登录相关")
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private SendSmsReigisterConfig sendSmsReigisterConfig;

    @Autowired
    private SendSmsLoginConfig sendSmsLoginConfig;

    @Autowired
    private RedisUtils redisUtils;

    @Reference
    private UserService userService;

    @Autowired
    private WxLoginConfig wxLoginConfig;


    @ApiOperation(value = "注册/微信绑定发送验证码")
    @GetMapping(value = "/toRegister")
    public ReturnResult toRegister(@RequestParam String phoneNumber) throws Exception {
        String registerUrl = sendSmsReigisterConfig.signUrl(phoneNumber);
        String codeBack = HttpClientUtils.doGet(registerUrl);
        JSONObject jsonObject = JSONObject.parseObject(codeBack);
        if (("OK").equals(jsonObject.getString("Code")) && ("OK").equals(jsonObject.getString("Message"))) {
            return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, SEND_CODE_SUCCESS);
        }
        return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, SEND_CODE_FAIL);
    }

    @ApiOperation(value = "注册")
    @GetMapping(value = "/register")
    public ReturnResult register(@Validated UserParam userParam) throws NoSuchAlgorithmException {
        String codeNew = redisUtils.get(REGISTER_PHONE_SMSCODE + userParam.getPhoneNumber()).toString();
        if (StringUtils.isEmpty(codeNew)) {
            return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, ERROR_PHONENUMBER_CODE);
        } else {
            if (codeNew.equals(userParam.getCode())) {
                if (userService.register(userParam)) {
                    return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, LOGIN_REGISTER_SUCCESS);
                } else {
                    return ReturnResultUtils.returnSuccess(FAIL_STATUS_CODE, LOGIN_AGAIN);
                }
            } else {
                return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, ERROR_CODE);
            }
        }
    }

    @ApiOperation(value = "登录发送验证码")
    @GetMapping(value = "/toLoginByCode")
    public ReturnResult toLoginByCode(@RequestParam String phoneNumber) throws Exception {
        String registerUrl = sendSmsLoginConfig.signUrl(phoneNumber);
        String codeBack = HttpClientUtils.doGet(registerUrl);
        JSONObject jsonObject = JSONObject.parseObject(codeBack);
        if (("OK").equals(jsonObject.getString("Code")) && ("OK").equals(jsonObject.getString("Message"))) {
            return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, SEND_CODE_SUCCESS);
        }
        return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, SEND_CODE_FAIL);
    }

    @ApiOperation(value = "验证码登录")
    @GetMapping(value = "/LoginByCode")
    public ReturnResult LoginByCode(@RequestParam String code, @RequestParam String phoneNumber, HttpServletRequest request) {
        if (userService.checkPhoneNumber(phoneNumber)) {
            String token = request.getSession().getId();
            String codeNew = redisUtils.get(LOGIN_PHONE_SMSCODE + phoneNumber).toString();
            if (StringUtils.isEmpty(codeNew)) {
                return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, ERROR_PHONENUMBER_CODE);
            } else {
                if (codeNew.equals(code)) {
                    redisUtils.set(token, userService.getUser(phoneNumber), 20000);
                    return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, LOGIN_REGISTER_SUCCESS, token);
                } else {
                    return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, ERROR_CODE);
                }
            }
        } else {
            return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, USER_NOT_EXIST);
        }
    }

    @ApiOperation(value = "密码登录")
    @GetMapping(value = "/loginByPwd")
    public ReturnResult loginByPwd(@RequestParam String phoneNumber, @RequestParam String password, HttpServletRequest request) throws NoSuchAlgorithmException {
        String token = request.getSession().getId();
        switch (userService.loginByPwd(phoneNumber, password, token)) {
            case 1:
                return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, USER_NOT_EXIST_OR_PWD_ERROR);
            case 0:
                return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, USER_LOGIN_SUCCESS, token);
        }
        return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, UNKNOWN_ERROR);
    }

    @ApiOperation(value = "微信登录")
    @GetMapping(value = "/wxLogin")
    public String wxLogin() {
        return wxLoginConfig.getCodeUrl();
    }

    @ApiIgnore
    @RequestMapping(value = "/callBack")
    public String callBack(String code) throws IOException {
        String accessTokenJsonStr = HttpClientUtils.doGet(wxLoginConfig.getAccessTokenUrl(code));
        JSONObject jsonObject = JSONObject.parseObject(accessTokenJsonStr);
        String accessToken = jsonObject.getString("access_token");
        String openid = jsonObject.getString("openid");
        String userInfo = HttpClientUtils.doGet(wxLoginConfig.getUserInfoUrl(accessToken, openid));
        JSONObject userInfoJson = JSONObject.parseObject(userInfo);
        return "redirect:+前台?openid=" + userInfoJson.getString("openid");
    }

    @ApiOperation(value = "检测微信登陆是否绑定手机号")
    @GetMapping(value = "/checkWxUser")
    public ReturnResult checkWxUser(@RequestParam String openid, HttpServletRequest request) {
        String token = request.getSession().getId();
        boolean flag = userService.checkWxUser(openid);
        if (!flag) {
            return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, BIND_WX_PHONE);
        }
        //微信登陆成功
        redisUtils.set(token, userService.getPhoneNumber(openid, token), 20000);
        return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, LOGIN_REGISTER_SUCCESS, token);
    }

    @ApiOperation(value = "微信绑定手机号")
    @GetMapping(value = "/wxBindPhone")
    public ReturnResult wxBindPhone(@RequestParam String openid, @RequestParam String phoneNumber, HttpServletRequest request) {
        String token = request.getSession().getId();
        int row = userService.wxBindPhone(openid, phoneNumber, token);
        if (row > 0) {
            return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, BIND_WX_PHONE_SUCCESS, token);
        } else {
            redisUtils.del(token);
            return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, BIND_WX_PHONE_FAIL);
        }
    }

    @ApiOperation(value = "退出登录")
    @GetMapping(value = "/quitLogin")
    public ReturnResult quitLogin(HttpServletRequest request) {
        String token = request.getSession().getId();
        try {
            redisUtils.del(token);
            return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, QUIT_LOGIN_SUCCESS);
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, QUIT_LOGIN_FAIL);
        }
    }


}
