package com.parada.consumer.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.parada.common.config.interAPI.CurrentUser;
import com.parada.common.config.interAPI.LoginRequired;
import com.parada.common.service.CollectService;
import com.parada.common.utils.ReturnResult;
import com.parada.common.utils.ReturnResultUtils;
import com.parada.common.vo.CollectclothesfileVo;
import com.parada.common.vo.UsersVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/collectController")
@Api(tags = "收藏")
public class CollectController {

    @Reference
    private CollectService collectService;

    @LoginRequired
    @GetMapping(value = "/collectClothe")
    @ApiOperation(value = "收藏商品")
    public ReturnResult<CollectclothesfileVo> collectClothe(@CurrentUser UsersVo usersVo, @RequestParam String clothesId) {
        if (collectService.collectClothe(usersVo.getUserId(), clothesId) > 0) {
            return ReturnResultUtils.returnSuccess(200, "收藏商品成功！", collectService.collectClothe(usersVo.getUserId(), clothesId));

        }
        return ReturnResultUtils.returnFail(400, "该商品已经被收藏，请勿重复操作！");
    }

    @LoginRequired
    @GetMapping(value = "/showCollectList")
    @ApiOperation(value = "展示收藏")
    public ReturnResult<List<CollectclothesfileVo>> showCollectList(@CurrentUser UsersVo usersVo) {
        if (collectService.showCollectList(usersVo.getUserId()).size() > 0) {
            return ReturnResultUtils.returnSuccess(200, "展示商品列表！", collectService.showCollectList(usersVo.getUserId()));

        } else {
            return ReturnResultUtils.returnFail(400, "商品收藏为空！");
        }

    }

    @LoginRequired
    @GetMapping(value = "/removeCollect")
    @ApiOperation(value = "删除收藏")
    public ReturnResult removeCollect(@CurrentUser UsersVo usersVo, @RequestParam String clothesId) {
        if (collectService.removeCollect(usersVo.getUserId(), clothesId) > 0) {
            return ReturnResultUtils.returnSuccess(200, "删除成功！", collectService.removeCollect(usersVo.getUserId(), clothesId));
        }
        return ReturnResultUtils.returnFail(400, "无此数据，删除失败！");
    }
}
