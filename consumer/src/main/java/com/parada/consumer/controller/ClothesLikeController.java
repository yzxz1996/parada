package com.parada.consumer.controller;

import com.parada.common.config.interAPI.CurrentUser;
import com.parada.common.config.interAPI.LoginRequired;
import com.parada.common.utils.RedisUtils;
import com.parada.common.vo.UsersVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.parada.common.cons.SupportEntity.*;

/**
 * create by Leo 2020/07/20
 */
@Api(tags = "商品点赞")
@RestController
@RequestMapping(value = "/clothesLike")
public class ClothesLikeController {

    @Autowired
    private RedisUtils redisUtils;

    @LoginRequired
    @ApiOperation(value = "商品点赞")
    @GetMapping(value = "/toLike")
    public Map<String, Object> toLike(@RequestParam Integer isLike, @RequestParam String clothesId, @CurrentUser UsersVo usersVo) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        boolean cheFreq = redisUtils.checkFreq(CHECK_REQ_NAMESPACE + clothesId + ":" + usersVo.getUserId(), 4, 5);
        if (!cheFreq) {
            resultMap.put("msg", "请勿频繁操作");
            resultMap.put("code", "491");
            return resultMap;
        }
        boolean checkIsLike = isLike(clothesId, usersVo.getUserId());
        if (isLike == +1 && checkIsLike) {
            resultMap.put("msg", "你已经点过赞了");
            resultMap.put("code", "490");
            return resultMap;
        }

        //保存用户点赞记录
        redisUtils.lSet(LIKE_HISTORY_LIST + clothesId + ":" + usersVo.getUserId(), isLike.toString());
        //保存这个商品所有用户的点赞量
        redisUtils.incr(GOODS_LIKE_SUM + clothesId, isLike);

        resultMap.put("isLike", checkIsLike);
        resultMap.put("goodsLikeSum", redisUtils.get(GOODS_LIKE_SUM + clothesId));
        return resultMap;
    }


    public boolean isLike(String goodsId, String userId) {
        List<String> likeList = (List<String>) (List) redisUtils.lGet(LIKE_HISTORY_LIST + goodsId + ":" + userId, 0, -1);
        return likeList.stream().map(Integer::parseInt).collect(Collectors.toList()).stream().mapToInt(Integer::intValue).sum() > 0;
    }

}
