package com.parada.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.parada.common.config.interAPI.CurrentUser;
import com.parada.common.config.interAPI.LoginRequired;
import com.parada.common.service.CouponService;
import com.parada.common.utils.RedisUtils;
import com.parada.common.utils.ReturnResult;
import com.parada.common.utils.ReturnResultUtils;
import com.parada.common.vo.CouponVo;
import com.parada.common.vo.UsersVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by CuiGeGe 2020/07/16
 */
@RestController
@RequestMapping(value = "/CouponController")
@Api(tags = "领取优惠券")
public class CouponController {
    @Reference
    private CouponService couponService;

    @Autowired
    private RedisUtils redisUtils;

    //userId 用户ID，disId 优惠券ID
    @LoginRequired
    @GetMapping(value = "/couponReceive")
    @ApiOperation(value = "领取优惠券")
    public ReturnResult couponReceive(@CurrentUser UsersVo usersVo, @RequestParam Integer disId) {

        if (null == usersVo.getUserId()){
            return ReturnResultUtils.returnFail(400, "请登录！");
        }
        boolean flag = couponService.couponReceive(usersVo.getUserId(), disId);
        if (flag) {
            return ReturnResultUtils.returnFail(200, "优惠券领取成功！");
        }

        return ReturnResultUtils.returnSuccess(400, "领取优惠券失败！");

    }

    //userId 用户ID
    @LoginRequired
    @GetMapping(value = "/showCoupon")
    @ApiOperation(value = "展示优惠券")
    public ReturnResult<CouponVo> showCoupon(@CurrentUser UsersVo usersVo) {

        return ReturnResultUtils.returnSuccess(200, "展示优惠券", couponService.showCoupon(usersVo.getUserId()));
    }
}
