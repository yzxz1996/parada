package com.parada.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.parada.common.config.interAPI.CurrentUser;
import com.parada.common.config.interAPI.LoginRequired;
import com.parada.common.service.ConsultService;
import com.parada.common.utils.ReturnResult;
import com.parada.common.utils.ReturnResultUtils;
import com.parada.common.vo.ConsultVo;
import com.parada.common.vo.UsersVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * create by CuiGeGe 2020/07/17
 */
@RestController
@RequestMapping(value = "/ConsultController")
@Api(tags = "用户咨询")
public class ConsultController {
    @Reference
    private ConsultService consultService;

    @LoginRequired
    @GetMapping(value = "/replyConsult")
    @ApiOperation(value = "咨询")
    public ReturnResult<ConsultVo> replyConsult(@CurrentUser UsersVo usersVo, String details) {

        return ReturnResultUtils.returnSuccess(200, "返回咨询列表", consultService.userConsult(usersVo.getUserId(), details));
    }
}
