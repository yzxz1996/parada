package com.parada.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.parada.common.config.interAPI.CurrentUser;
import com.parada.common.config.interAPI.LoginRequired;
import com.parada.common.param.UsersInfoParam;
import com.parada.common.service.UserInfoService;
import com.parada.common.utils.ReturnResult;
import com.parada.common.utils.ReturnResultUtils;
import com.parada.common.vo.UserInfoVo;
import com.parada.common.vo.UsersVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.parada.common.cons.SupportEntity.*;

/**
 * create by Leo 2020/07/14
 */
@Api(tags = "个人信息展示")
@RestController
@RequestMapping(value = "/userInfo")
public class UserInfoController {

    @Reference
    private UserInfoService userInfoService;


    @ApiOperation(value = "个人信息展示")
    @LoginRequired
    @GetMapping(value = "/userInfoExhibition")
    public ReturnResult<UserInfoVo> userInfoExhibition(@CurrentUser UsersVo usersVo) {
        try {
            UserInfoVo userInfoVo = userInfoService.showUser(usersVo.getUserId());
            return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, USERINFO_EXHIBITION, userInfoVo);
        } catch (Exception e) {
            return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, e.getMessage());
        }
    }

    @ApiOperation(value = "个人信息修改")
    @LoginRequired
    @GetMapping(value = "/modifyUserInfo")
    public ReturnResult modifyUserInfo(@Validated UsersInfoParam usersInfoParam, @CurrentUser UsersVo usersVo) {
        int row = userInfoService.modifyUserInfo(usersVo, usersInfoParam);
        if (row > 0) {
            return ReturnResultUtils.returnSuccess(SUCCESS_STATUS_CODE, MODIFY_USERINFO_SUCCESS);
        } else {
            return ReturnResultUtils.returnFail(FAIL_STATUS_CODE, MODIFY_USERINFO_FAIL);
        }
    }
}
