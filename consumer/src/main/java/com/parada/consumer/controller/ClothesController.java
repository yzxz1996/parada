package com.parada.consumer.controller;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.parada.common.config.interAPI.CurrentUser;
import com.parada.common.config.interAPI.LoginRequired;
import com.parada.common.service.HotWordService;
import com.parada.common.service.ScoresService;
import com.parada.common.service.ShopByIpService;
import com.parada.common.service.ShowClothesService;
import com.parada.common.utils.IpAndAddrUtils;
import com.parada.common.utils.RedisUtils;
import com.parada.common.utils.ReturnResult;
import com.parada.common.utils.ReturnResultUtils;
import com.parada.common.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

/**
 * Created by yuhw on 2020/7/13
 */
@Log4j
@Api(tags = "商品相关")
@RestController
@RequestMapping(value = "/clothes")
public class ClothesController {


    @Reference
    private ShopByIpService shopByIpService;

    @Autowired
    private IpAndAddrUtils ipAndAddrUtils;

    @Reference
    private ShowClothesService showClothesService;

    @Reference
    private HotWordService hotWordService;

    @Reference
    private ScoresService scoresService;

    @Autowired
    private RedisUtils redisUtils;


    @ApiOperation(value = "获得ip根据ip定位信息")
    @GetMapping(value = "/getIp")
    public String getIp(HttpServletRequest request) throws Exception {
        String[] cmds = {"curl", "icanhazip.com"};
        String ip = ipAndAddrUtils.execCurl(cmds).trim();
        String address = "";
        if (ip.length() > 0) {
            address = ipAndAddrUtils.getAddressResult(ip);
        }
        return address;
    }

    @ApiOperation(value = "根据商品名模糊搜索商品列表")
    @GetMapping(value = "/showClothesByName")
    public List<ClothesDetailsVo> showClothesByName(@ApiParam(value = "页数") @RequestParam int pageNo, @ApiParam(value = "商品名称") @RequestParam String clothesName, @ApiParam(value = "商品分类") @RequestParam String clothesType) throws ParseException {
        //添加搜索词汇进数据库
        hotWordService.addHotWord(clothesName);
        //执行查询并且返回结果
        return showClothesService.showClothesByName(pageNo, clothesName, clothesType);
    }

    @ApiOperation(value = "根据商品分类展示商品列表")
    @GetMapping(value = "/showClothesByType")
    public List<ClothesDetailsVo> showClothesByType(@ApiParam(value = "页数") @RequestParam int pageNo, @ApiParam(value = "商品分类") @RequestParam String clothesType) throws ParseException {
        return showClothesService.showClothesByType(pageNo, clothesType);
    }

    @ApiOperation(value = "根据性别展示商品分类")
    @GetMapping(value = "/showClothesBySex")
    public List<ClothesKindVo> showClothesBySex(@ApiParam(value = "商品性别分类") @RequestParam int sexId) {
        return showClothesService.showClothesBySex(sexId);
    }

    @ApiOperation(value = "根据商品id展示商品详情")
    @GetMapping(value = "/showClothesMsg")
    public ClothesDetailsVoMsg showClothesMsg(@ApiParam(value = "商品id") @RequestParam String clothesId) {
        return showClothesService.showClothesMsg(clothesId);
    }

    @ApiOperation(value = "检查此ip下有无购车，有就删除，返回true，无就返回false")
    @PostMapping(value = "/delCarByIp")
    public boolean delCarByIp(HttpServletRequest request) {
        String ip = ipAndAddrUtils.getIp(request);
        return shopByIpService.delCarByIp(ip);
    }

    @ApiOperation(value = "判断是否登录")
    @GetMapping(value = "/checkIfLogin")
    public String checkIfLogin(HttpServletRequest request) {
        String newToken = request.getHeader("token");
        if (ObjectUtils.isEmpty(redisUtils.get(newToken))) {
            return "redirect:localhost:1997/clothes/shopByIp";
        } else {
            return "redirect:localhost:1997/clothes/addCarByUserId";
        }
    }


    @ApiOperation(value = "根据ip添加购物车")
    @PostMapping(value = "/shopByIp")
    public ReturnResult shopByIp(@ApiParam(value = "商品id") @RequestParam String clothesId, @ApiParam(value = "购买数量") @RequestParam int count, HttpServletRequest request) {
        String ip = ipAndAddrUtils.getIp(request);
        int row = shopByIpService.addCarByIp(ip, clothesId, count);
        if (row > 0) {
            return ReturnResultUtils.returnSuccess(200, "添加购物车成功");
        } else {
            return ReturnResultUtils.returnFail(405, "添加购物车失败");
        }
    }

    @ApiOperation(value = "根据ip查看购物车")
    @PostMapping(value = "/lookCarByIp")
    public List<ShopcarVo> lookCarByIp(HttpServletRequest request) {
        String ip = ipAndAddrUtils.getIp(request);
        return shopByIpService.lookCarByIp(ip);
    }

    @LoginRequired
    @ApiOperation(value = "根据userId添加购物车")
    @PostMapping(value = "/addCarByUserId")
    public int addCarByUserId(@CurrentUser UsersVo usersVo, @ApiParam(value = "商品id") @RequestParam String clothesId, @ApiParam(value = "购买数量") @RequestParam int count, HttpServletRequest request) {
        String ip = ipAndAddrUtils.getIp(request);
        return showClothesService.addCarByUserId(usersVo.getUserId(), ip, clothesId, count);
    }

    @ApiOperation(value = "展示十条热搜词")
    @PostMapping(value = "/showHotWord")
    public List<HotwordVo> showHotWord() {
        return hotWordService.showHotWord();
    }

    @LoginRequired
    @ApiOperation(value = "收货之后在评价")
    @PostMapping(value = "/addSores")
    public ReturnResult addScores(@CurrentUser UsersVo usersVo, @ApiParam(value = "商品id") @RequestParam String clothesId, @ApiParam(value = "商品评价") @RequestParam String assess, @ApiParam(value = "商品评分") @RequestParam int scores) {
        if (scoresService.addScores(usersVo.getUserId(), clothesId, assess, scores)) {
            return ReturnResultUtils.returnSuccess(200, "评价成功");
        } else {
            return ReturnResultUtils.returnFail(405, "评价失败");
        }
    }

    @ApiOperation(value = "展示商品相对应的评价及评分")
    @PostMapping(value = "/showScores")
    public List<ScoresVo> showScores(@ApiParam(value = "商品id") @RequestParam String clothesId) {
        return scoresService.showScores(clothesId);
    }
}
