package com.parada.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.parada.common.service.BannerService;
import com.parada.common.utils.ReturnResult;
import com.parada.common.utils.ReturnResultUtils;
import com.parada.common.vo.BannerVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/banner")
@Api(tags = "轮播图链接")
public class BannerController {

    @Reference
    private BannerService bannerService;

    @GetMapping(value = "/queryBanner")
    @ApiOperation(value = "查询banner")
    public ReturnResult<BannerVo> queryBanner() {
        return ReturnResultUtils.returnSuccess(200, "查询banner内容成功！", bannerService.queryBanner());

    }
}
