package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.ShopByIpService;
import com.parada.common.vo.ShopcarVo;
import com.parada.provider.dto.ClothesDetailsDto;
import com.parada.provider.dto.ShopcarDto;
import com.parada.provider.dto.ShopcarExample;
import com.parada.provider.mapper.ClothesDetailsMapper;
import com.parada.provider.mapper.ShopcarMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuhw on 2020/7/13
 */
@Service
public class ShopByIpServiceImpl implements ShopByIpService {

//    @Autowired
//    private IpAndAddrUtils ipAndAddrUtils;

    @Autowired
    private ShopcarMapper shopcarMapper;

    @Autowired
    private ClothesDetailsMapper clothesdetailsMapper;

    @Override
    public boolean delCarByIp(String ip) {
        ShopcarExample shopcarExample = new ShopcarExample();
        shopcarExample.createCriteria().andIpaddressEqualTo(ip);
        List<ShopcarDto> shopcarDtos = shopcarMapper.selectByExample(shopcarExample);
        if (shopcarDtos.size() > 0) {
            shopcarMapper.deleteByExample(shopcarExample);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int addCarByIp(String ip, String clothesId, int count) {
        //判断库存是否足够
        ClothesDetailsDto clothesdetailsDto = clothesdetailsMapper.selectByPrimaryKey(clothesId);
        int i = clothesdetailsDto.getStock();
        if (i >= count) {
            ShopcarDto shopcarDto = new ShopcarDto();
            shopcarDto.setIpaddress(ip);
            shopcarDto.setClothesId(clothesId);
            shopcarDto.setCount(count);
            int row = shopcarMapper.insertSelective(shopcarDto);
            return row;
        } else {
            return 0;
        }

    }

    //userId查看购物车
    @Override
    public List<ShopcarVo> lookCarByUserId(String userId) {
        ShopcarExample shopcarExample = new ShopcarExample();
        shopcarExample.createCriteria().andUserIdEqualTo(userId);
        List<ShopcarDto> shopcarDtos = shopcarMapper.selectByExample(shopcarExample);
        List<ShopcarVo> shopcarVos = new ArrayList<ShopcarVo>();
        shopcarDtos.forEach(shopcarDto -> {
            ShopcarVo shopcarVo = new ShopcarVo();
            BeanUtils.copyProperties(shopcarDto, shopcarVo);
            shopcarVos.add(shopcarVo);
            //根据购物车中商品的商品id查商品的价格，然后塞到购物车对应的商品上
            ClothesDetailsDto clothesdetailsDto = clothesdetailsMapper.selectByPrimaryKey(shopcarVo.getClothesId());
            shopcarVo.setPrice(clothesdetailsDto.getPrice());
            shopcarVo.setTotalPrice(clothesdetailsDto.getPrice() * shopcarVo.getCount());
        });
        return shopcarVos;
    }

    //ip查看购物车
    @Override
    public List<ShopcarVo> lookCarByIp(String ip) {
        //todo 判断是否登录，如果登录，通过userId查看购物车，如果没有登录，则通过ip查看购物车
        ShopcarExample shopcarExample = new ShopcarExample();
        shopcarExample.createCriteria().andIpaddressEqualTo(ip);
        List<ShopcarDto> shopcarDtos = shopcarMapper.selectByExample(shopcarExample);
        List<ShopcarVo> shopcarVos = new ArrayList<ShopcarVo>();
        shopcarDtos.forEach(shopcarDto -> {
            ShopcarVo shopcarVo = new ShopcarVo();
            BeanUtils.copyProperties(shopcarDto, shopcarVo);
            shopcarVos.add(shopcarVo);
            //根据购物车中商品的商品id查商品的价格，然后塞到购物车对应的商品上
            ClothesDetailsDto clothesdetailsDto = clothesdetailsMapper.selectByPrimaryKey(shopcarVo.getClothesId());
            shopcarVo.setPrice(clothesdetailsDto.getPrice());
            shopcarVo.setTotalPrice(clothesdetailsDto.getPrice() * shopcarVo.getCount());
        });
        return shopcarVos;
    }
}
