package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.ShowClothesService;
import com.parada.common.utils.PageUtils;
import com.parada.common.utils.RedisUtils;
import com.parada.common.vo.ClothesDetailsVo;
import com.parada.common.vo.ClothesDetailsVoMsg;
import com.parada.common.vo.ClothesKindVo;
import com.parada.common.vo.ShopcarVo;
import com.parada.provider.dto.*;
import com.parada.provider.mapper.ClothesDetailsMapper;
import com.parada.provider.mapper.ClothesKindMapper;
import com.parada.provider.mapper.ScoresMapper;
import com.parada.provider.mapper.ShopcarMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by yuhw on 2020/7/13
 */
@Service
public class ShowClothesServiceImpl implements ShowClothesService {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private ClothesDetailsMapper clothesdetailsMapper;

    @Autowired
    private ClothesKindMapper clotheskindMapper;

    @Autowired
    private ShopcarMapper shopcarMapper;

    @Autowired
    private ScoresMapper scoresMapper;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");


    //根据名字模糊查询（可以在分类下查）
    @Override
    public List<ClothesDetailsVo> showClothesByName(int pageNo, String clothesName, String clothesType) throws ParseException {
        ClothesDetailsExample clothesdetailsExample = new ClothesDetailsExample();
        clothesdetailsExample.createCriteria().andClothesNameLike("%" + clothesName + "%")
                .andClothesTypeEqualTo(clothesType);
        return page(pageNo, clothesdetailsExample);
    }

    //根据种类展示所有分类下的商品列表
    @Override
    public List<ClothesDetailsVo> showClothesByType(int pageNo, String clothesType) throws ParseException {
        ClothesDetailsExample clothesdetailsExample = new ClothesDetailsExample();
        clothesdetailsExample.createCriteria().andClothesTypeEqualTo(clothesType);
        return page(pageNo, clothesdetailsExample);
    }

    //分页查询
    public List<ClothesDetailsVo> page(int pageNo, ClothesDetailsExample clothesdetailsExample) throws ParseException {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setTotalCount(clothesdetailsMapper.countByExample(clothesdetailsExample));
        clothesdetailsExample.setLimit(pageUtils.getPageNo());
        clothesdetailsExample.setOffset(pageUtils.getPageSize());
        List<ClothesDetailsDto> clothesDetailsDtos = clothesdetailsMapper.selectByExample(clothesdetailsExample);
        List<ClothesDetailsVo> clothesDetailsVos = new ArrayList<ClothesDetailsVo>();
        clothesDetailsDtos.forEach(clothesDetailsDto1 -> {
            ClothesDetailsVo clothesdetailsVo = new ClothesDetailsVo();
            BeanUtils.copyProperties(clothesDetailsDto1, clothesdetailsVo);
            clothesDetailsVos.add(clothesdetailsVo);
        });
        return clothesDetailsVos;
    }

    //根据性别分类展示分类下所有商品种类
    @Override
    public List<ClothesKindVo> showClothesBySex(int sexId) {
        ClothesKindExample clotheskindExample = new ClothesKindExample();
        clotheskindExample.createCriteria().andSexIdEqualTo(sexId);
        List<ClothesKindDto> clothesKindDtos = clotheskindMapper.selectByExample(clotheskindExample);
        List<ClothesKindVo> clothesKindVos = new ArrayList<ClothesKindVo>();
        clothesKindDtos.forEach(clothesKindDto -> {
            ClothesKindVo clotheskindVo = new ClothesKindVo();
            BeanUtils.copyProperties(clothesKindDto, clotheskindVo);
            clothesKindVos.add(clotheskindVo);
        });
        return clothesKindVos;
    }

    //根据商品id展示商品详情
    @Override
    public ClothesDetailsVoMsg showClothesMsg(String clothesId) {
        ClothesDetailsDto clothesdetailsDto = clothesdetailsMapper.selectByPrimaryKey(clothesId);
        ClothesDetailsVoMsg clothesdetailsVoMsg = new ClothesDetailsVoMsg();
        BeanUtils.copyProperties(clothesdetailsDto, clothesdetailsVoMsg);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        clothesdetailsVoMsg.setCreateTimee(sdf.format(clothesdetailsDto.getCreateTime()));
        clothesdetailsVoMsg.setUpdateTimee(sdf.format(clothesdetailsDto.getUpdateTime()));
        ScoresExample scoresExample = new ScoresExample();
        scoresExample.createCriteria().andClothesIdEqualTo(clothesId);
        List<ScoresDto> scoresDtos = scoresMapper.selectByExample(scoresExample);
        double scoresSum = (double) scoresDtos.stream().mapToInt(p -> p.getScore()).sum();
        long i = scoresMapper.countByExample(scoresExample);
        double scoresAvg = scoresSum / i;
        DecimalFormat df = new DecimalFormat("0.0");
        clothesdetailsVoMsg.setScores(df.format(scoresAvg));
        return clothesdetailsVoMsg;
    }


    @Override
    public int addCarByUserId(String userId, String ip, String clothesId, int count) {
        //判断库存是否足够
        ClothesDetailsDto clothesdetailsDto = clothesdetailsMapper.selectByPrimaryKey(clothesId);
        int i = clothesdetailsDto.getStock();
        if (i >= count) {
            //已经在ip添加过购物车的情况（把userId更新进去，把ip变为空）
            ShopcarExample shopcarExample = new ShopcarExample();
            shopcarExample.createCriteria().andIpaddressEqualTo(ip);
            List<ShopcarDto> shopcarDtos = shopcarMapper.selectByExample(shopcarExample);
            if (shopcarDtos.size() > 0) {
                ShopcarExample shopcarExample1 = new ShopcarExample();
                shopcarExample1.createCriteria().andIpaddressEqualTo(ip);
                List<ShopcarDto> shopcarDtos1 = shopcarMapper.selectByExample(shopcarExample1);
                ShopcarDto shopcarDto = new ShopcarDto();
                shopcarDto.setUserId(userId);
                shopcarDto.setIpaddress(null);
                shopcarDto.setCount(count);
                shopcarDto.setClothesId(clothesId);
                int row = shopcarMapper.updateByExample(shopcarDto, shopcarExample1);
                return row;
            } else {
                //没有在ip添加购物车，直接根据userId添加
                ShopcarDto shopcarDto = new ShopcarDto();
                shopcarDto.setUserId(userId);
                shopcarDto.setClothesId(clothesId);
                shopcarDto.setCount(count);
                int row = shopcarMapper.insertSelective(shopcarDto);
                return row;
            }
        } else {
            return 0;
        }

    }

    @Override
    public List<ShopcarVo> lookCarByUesrId(String userId) {
        ShopcarExample shopcarExample = new ShopcarExample();
        shopcarExample.createCriteria().andUserIdEqualTo(userId);
        List<ShopcarDto> shopcarDtos = shopcarMapper.selectByExample(shopcarExample);
        List<ShopcarVo> shopcarVos = new ArrayList<ShopcarVo>();
        shopcarDtos.forEach(shopcarDto -> {
            ShopcarVo shopcarVo = new ShopcarVo();
            BeanUtils.copyProperties(shopcarDto, shopcarVo);
            shopcarVos.add(shopcarVo);
            ClothesKindVo clotheskindVo = new ClothesKindVo();
            //根据购物车中商品的商品id查商品的价格，然后塞到购物车对应的商品上
            ClothesDetailsDto clothesdetailsDto = clothesdetailsMapper.selectByPrimaryKey(shopcarVo.getClothesId());
            shopcarVo.setPrice(clothesdetailsDto.getPrice());
            shopcarVo.setTotalPrice(clothesdetailsDto.getPrice() * shopcarVo.getCount());
        });
        return shopcarVos;

    }
}
