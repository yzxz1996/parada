package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.ScoresService;
import com.parada.common.utils.ReturnResult;
import com.parada.common.utils.ReturnResultUtils;
import com.parada.common.vo.ScoresVo;
import com.parada.provider.dto.ScoresDto;
import com.parada.provider.dto.ScoresExample;
import com.parada.provider.mapper.ScoresMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuhw on 2020/7/15
 */
@Service
public class ScoresServiceImpl implements ScoresService {

    @Autowired
    private ScoresMapper scoresMapper;

    @Override
    public boolean addScores(String userId, String clothesId, String assess, int scores) {
        //todo 判断订单是否付款，已付款进入以下流程
        ScoresDto scoresDto = new ScoresDto();
        scoresDto.setClothesId(clothesId);
        scoresDto.setAssess(assess);
        scoresDto.setScore(scores);
        scoresDto.setUserId(userId);
        int i = scoresMapper.insertSelective(scoresDto);
        if (i > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<ScoresVo> showScores(String clothesId) {
        ScoresExample scoresExample = new ScoresExample();
        scoresExample.createCriteria().andClothesIdEqualTo(clothesId);
        List<ScoresDto> scoresDtos = scoresMapper.selectByExample(scoresExample);
        List<ScoresVo> scoresVos = new ArrayList<ScoresVo>();
        scoresDtos.forEach(scoresDto -> {
            ScoresVo scoresVo = new ScoresVo();
            BeanUtils.copyProperties(scoresDto, scoresVo);
            scoresVos.add(scoresVo);
        });
        return scoresVos;
    }
}
