package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.CouponService;
import com.parada.common.utils.RedisUtils;
import com.parada.common.vo.CouponVo;
import com.parada.provider.dto.CouponDto;
import com.parada.provider.dto.CouponExample;
import com.parada.provider.dto.CouponKindDto;
import com.parada.provider.mapper.CouponKindMapper;
import com.parada.provider.mapper.CouponMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * create by CuiGeGe 2020/07/16
 */
@Service
public class CouponServiceImpl implements CouponService {
    private String couponReceive = "couponReceive:";
    @Autowired
    private CouponMapper couponMapper;
    @Autowired
    private CouponKindMapper couponKindMapper;
    @Autowired
    private RedisUtils redisUtils;


    //领取优惠券
    @Override
    public boolean couponReceive(String userId, Integer disId) {
        List<Object> disIds = redisUtils.lGet(couponReceive + userId, 0, -1);
        if (CollectionUtils.isEmpty(disIds) || !disIds.contains(disId)) {
            redisUtils.lSet(couponReceive + userId, disId);
            CouponDto couponDto = new CouponDto();
            couponDto.setDisId(disId);
            couponDto.setUserId(userId);
            int num = couponMapper.insertSelective(couponDto);
            if (num > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<CouponVo> showCoupon(String userId) {
        CouponExample couponExample = new CouponExample();
        couponExample.createCriteria().andUserIdEqualTo(userId);
        List<CouponDto> couponDtos = couponMapper.selectByExample(couponExample);
        List<CouponVo> couponVos = new ArrayList<CouponVo>();
        couponDtos.forEach(couponDto -> {
            CouponVo couponVo = new CouponVo();
            BeanUtils.copyProperties(couponDto, couponVo);
            CouponKindDto couponKindDto = couponKindMapper.selectByPrimaryKey(couponVo.getDisId());
            //此处空指针，需要改
            if (couponKindDto != null) {
                couponVo.setClothesKind(couponKindDto.getClothesKind());
                couponVo.setCouponPrice(couponKindDto.getCouponPrice());
                couponVo.setCouponSubPrice(couponKindDto.getCouponSubPrice());
                couponVo.setCreateTime(couponKindDto.getCreateTime());
                couponVo.setValidate(couponKindDto.getValidate());
                couponVos.add(couponVo);
            }
        });
        return couponVos;
    }


}

