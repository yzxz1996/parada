package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.HotWordService;
import com.parada.common.vo.HotwordVo;
import com.parada.provider.dto.HotwordDto;
import com.parada.provider.dto.HotwordExample;
import com.parada.provider.mapper.HotwordMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuhw on 2020/7/15
 */
@Service
public class HotWordServiceImpl implements HotWordService {

    @Autowired
    private HotwordMapper hotwordMapper;

    @Override
    public void addHotWord(String clothesName) {
        HotwordExample hotwordExample = new HotwordExample();
        hotwordExample.createCriteria().andWordNameEqualTo(clothesName);
        List<HotwordDto> hotwordDtos = hotwordMapper.selectByExample(hotwordExample);
        if (hotwordDtos.size() <= 0) {
            HotwordDto hotwordDto = new HotwordDto();
            hotwordDto.setWordName(clothesName);
            hotwordDto.setCount(1);
            hotwordMapper.insert(hotwordDto);
        } else {
            HotwordDto hotwordDto = new HotwordDto();
            hotwordDto.setWordName(clothesName);
            hotwordDto.setCount(hotwordDtos.get(0).getCount() + 1);
            hotwordMapper.updateByExampleSelective(hotwordDto, hotwordExample);
        }
    }

    @Override
    public List<HotwordVo> showHotWord() {
        HotwordExample hotwordExample = new HotwordExample();
        hotwordExample.createCriteria().andWordNameIsNotNull();
        List<HotwordDto> hotwordDtos = hotwordMapper.selectByExample(hotwordExample);
        List<HotwordVo> hotwordVos = new ArrayList<HotwordVo>();
        hotwordDtos.forEach(hotwordDto -> {
            HotwordVo hotwordVo = new HotwordVo();
            BeanUtils.copyProperties(hotwordDto, hotwordVo);
            SimpleDateFormat sdf = new SimpleDateFormat();
            hotwordVo.setCreateTimee(sdf.format(hotwordDto.getCreateTime()));
            hotwordVos.add(hotwordVo);
        });
        return hotwordVos;
    }
}
