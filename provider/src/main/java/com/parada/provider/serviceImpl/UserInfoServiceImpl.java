package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.param.UsersInfoParam;
import com.parada.common.service.UserInfoService;
import com.parada.common.vo.UserInfoVo;
import com.parada.common.vo.UsersVo;
import com.parada.provider.dto.UsersDto;
import com.parada.provider.dto.UsersExample;
import com.parada.provider.mapper.UsersMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * create by Leo 2020/07/14
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {
    @Autowired
    private UsersMapper usersMapper;


    @Override
    public UserInfoVo showUser(String userId) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andUserIdEqualTo(userId);
        List<UsersDto> usersDtos = usersMapper.selectByExample(usersExample);
        UserInfoVo userInfoVo = new UserInfoVo();
        BeanUtils.copyProperties(usersDtos.get(0), userInfoVo);
        return userInfoVo;
    }

    @Override
    public Integer modifyUserInfo(UsersVo usersVo, UsersInfoParam usersInfoParam) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andUserIdEqualTo(usersVo.getUserId());
        UsersDto usersDto = new UsersDto();
        if (null != usersInfoParam.getNewPhoneNumber()) {
            usersDto.setPhoneNumber(usersInfoParam.getNewPhoneNumber());
        }
        if (null != usersInfoParam.getPassword()) {
            usersDto.setPassword(usersInfoParam.getPassword());
        }
        if (null != usersInfoParam.getName()) {
            usersDto.setName(usersInfoParam.getName());
        }
        if (null != usersInfoParam.getAddress()) {
            usersDto.setAddress(usersInfoParam.getAddress());
        }
        if (null != usersInfoParam.getAge()) {
            usersDto.setAge(usersInfoParam.getAge());
        }
        if (null != usersInfoParam.getImg()) {
            usersDto.setImg(usersInfoParam.getImg());
        }
        if (null != usersInfoParam.getCountry()) {
            usersDto.setCountry(usersInfoParam.getCountry());
        }
        if (null != usersInfoParam.getSex()) {
            usersDto.setSex(usersInfoParam.getSex());
        }
        if (null != usersInfoParam.getPCA()) {
            usersDto.setpCA(usersInfoParam.getPCA());
        }
        usersDto.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        return usersMapper.updateByExampleSelective(usersDto, usersExample);
    }


}
