package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.ConsultService;
import com.parada.common.vo.ConsultVo;
import com.parada.provider.dto.ConsultDto;
import com.parada.provider.dto.ConsultExample;
import com.parada.provider.mapper.ConsultMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * create by CuiGeGe 2020/07/17
 */
@Service
public class ConsultServiceImpl implements ConsultService {
    @Autowired
    private ConsultMapper consultMapper;

    @Override
    public List<ConsultVo> userConsult(String userId, String details) {
        ConsultDto consultDto = new ConsultDto();
        consultDto.setDetails(details);
        consultDto.setUserId(userId);
        int num = consultMapper.insertSelective(consultDto);
        ConsultExample consultExample = new ConsultExample();
        consultExample.createCriteria().andUserIdEqualTo(userId);
//        consultExample.setOrderByClause("create_time ASC");
        List<ConsultDto> consultDtos = consultMapper.selectByExample(consultExample);
        List<ConsultVo> consultVos = new ArrayList<ConsultVo>();
        consultDtos.forEach(consultDto1 -> {
            ConsultVo consultVo = new ConsultVo();
            BeanUtils.copyProperties(consultDto1, consultVo);
            consultVos.add(consultVo);

        });

        return consultVos;
    }
}
