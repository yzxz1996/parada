package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.BannerService;
import com.parada.common.vo.BannerVo;
import com.parada.provider.dto.BannerDto;
import com.parada.provider.mapper.BannerMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service
public class BannerServiceImpl implements BannerService {
    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public List<BannerVo> queryBanner() {
        List<BannerDto> bannerDto = bannerMapper.selectByExample(null);
        List<BannerVo> bannerVos = new ArrayList<BannerVo>();
        bannerDto.forEach(bannerDto1 -> {
            BannerVo bannerVo = new BannerVo();
            BeanUtils.copyProperties(bannerDto1, bannerVo);
            bannerVos.add(bannerVo);
        });
        return bannerVos;
    }
}
