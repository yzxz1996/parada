package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.ClothesOrderService;
import com.parada.common.utils.DateUtils;
import com.parada.common.utils.RedisUtils;
import com.parada.common.vo.ClothesOrderVo;
import com.parada.provider.dto.*;
import com.parada.provider.mapper.ClothesDetailsMapper;
import com.parada.provider.mapper.ClothesOrderMapper;
import com.parada.provider.mapper.CouponKindMapper;
import com.parada.provider.mapper.CouponMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static com.parada.common.cons.SupportEntity.*;

/**
 * create by Leo 2020/07/16
 */
@Service
public class ClothesOrderServiceImpl implements ClothesOrderService {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private ClothesDetailsMapper clothesDetailsMapper;

    @Autowired
    private ClothesOrderMapper clothesOrderMapper;

    @Autowired
    private CouponMapper couponMapper;

    @Autowired
    private CouponKindMapper couponKindMapper;


    @Override
    public boolean checkNum(ClothesOrderVo clothesOrderVo) {
        //拿oid-购买数量，拿gid-库存
        long buyNum = clothesOrderVo.getNum();
        ClothesDetailsDto clothesDetailDto = clothesDetailsMapper.selectByPrimaryKey(clothesOrderVo.getClothesId());
        if (!isLock(clothesOrderVo.getClothesId()) && buyNum > clothesDetailDto.getStock()) {
            return false;
        }
        return true;
    }

    @Override
    public void insertOrder(ClothesOrderVo clothesOrderVo) {
        ClothesOrderDto clothesOrderDto = new ClothesOrderDto();
        clothesOrderDto.setId(clothesOrderVo.getId());
        clothesOrderDto.setClothesId(clothesOrderVo.getClothesId());
        clothesOrderDto.setUserId(clothesOrderVo.getUserId());
        clothesOrderDto.setNumber(clothesOrderVo.getNum());
        ClothesDetailsExample clothesDetailExample = new ClothesDetailsExample();
        clothesDetailExample.createCriteria().andClothesIdEqualTo(clothesOrderVo.getClothesId());
        List<ClothesDetailsDto> clothesDetailDtos = clothesDetailsMapper.selectByExample(clothesDetailExample);
        if (CollectionUtils.isEmpty(clothesDetailDtos)) {
            return;
        }
        Double totalFee = (clothesDetailDtos.get(0).getPrice() * clothesOrderVo.getNum()) * 100;
        if (null == clothesOrderVo.getDisId()) {
            totalFee = (clothesDetailDtos.get(0).getPrice() * clothesOrderVo.getNum()) * 100;
        } else {
            CouponExample couponExample = new CouponExample();
            couponExample.createCriteria().andDisIdEqualTo(clothesOrderVo.getDisId()).andUserIdEqualTo(clothesOrderVo.getUserId());
            List<CouponDto> couponDtos = couponMapper.selectByExample(couponExample);
            if (CollectionUtils.isEmpty(couponDtos) || couponDtos.get(0).getStatus() != 0) {
            } else {
                CouponKindDto couponKindDto = couponKindMapper.selectByPrimaryKey(clothesOrderVo.getDisId());
                if (!clothesDetailDtos.get(0).getClothesType().equals(couponKindDto.getClothesKind())) {
                    return;
                } else {
                    if (couponKindDto.getCouponSubPrice() > totalFee) {
                        return;
                    } else {
                        int interval = DateUtils.getDaysGapOfDates(couponDtos.get(0).getCreateTime(), new Date());
                        if (interval > couponKindDto.getValidate()) {
                            return;
                        } else {
                            totalFee = (clothesDetailDtos.get(0).getPrice() * clothesOrderVo.getNum()) * 100 - couponKindDto.getCouponPrice();
                        }
                    }
                }
            }
        }
        clothesOrderDto.setTotalPrice(totalFee);
        clothesOrderMapper.insertSelective(clothesOrderDto);
    }

    @Override
    public void cutCountAndUpdateOrders(String orderId) {
        ClothesOrderExample clothesOrderExample = new ClothesOrderExample();
        clothesOrderExample.createCriteria().andIdEqualTo(orderId);
        List<ClothesOrderDto> clothesOrderDtos = clothesOrderMapper.selectByExample(clothesOrderExample);
        Integer count = clothesDetailsMapper.selectByPrimaryKey(clothesOrderDtos.get(0).getClothesId()).getStock();
        ClothesDetailsExample clothesDetailExample = new ClothesDetailsExample();
        clothesDetailExample.createCriteria().andClothesIdEqualTo(clothesOrderDtos.get(0).getClothesId());
        ClothesDetailsDto clothesDetailDto = new ClothesDetailsDto();
        clothesDetailDto.setStock(count - clothesOrderDtos.get(0).getNumber());
        clothesDetailDto.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        //send
        while (true) {
            if (redisUtils.lock(GOODS_COUNT_NAMESPACE_LOCK + clothesOrderDtos.get(0).getClothesId(), null, 10000000)) {
                clothesDetailsMapper.updateByExampleSelective(clothesDetailDto, clothesDetailExample);
                redisUtils.delLock(GOODS_COUNT_NAMESPACE_LOCK + clothesOrderDtos.get(0).getClothesId());
                break;
            }
        }
        //修改订单状态
        ClothesOrderDto clothesOrderDto = new ClothesOrderDto();
        clothesOrderDto.setId(clothesOrderDtos.get(0).getId());
        clothesOrderDto.setIsPay(1);
        clothesOrderMapper.updateByPrimaryKeySelective(clothesOrderDto);
    }

    //false 没上锁
    public boolean isLock(String gid) {
        return redisUtils.get(GOODS_COUNT_NAMESPACE_LOCK + gid) == null ? false : true;
    }
}
