package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.PayService;
import com.parada.common.utils.DateUtils;
import com.parada.common.utils.HttpClientUtils;
import com.parada.common.utils.UUIDUtils;
import com.parada.common.utils.WxPayUtils;
import com.parada.common.vo.ClothesOrderVo;
import com.parada.provider.config.WxPayConfig;
import com.parada.provider.dto.*;
import com.parada.provider.mapper.ClothesDetailsMapper;
import com.parada.provider.mapper.CouponKindMapper;
import com.parada.provider.mapper.CouponMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.util.*;

import static com.parada.common.cons.SupportEntity.*;

/**
 * create by Leo 2020/07/16
 */
@Service
public class PayServiceImpl implements PayService {

    @Autowired
    private WxPayConfig wxPayConfig;
    @Autowired
    private ClothesDetailsMapper clothesDetailsMapper;
    @Autowired
    private CouponMapper couponMapper;
    @Autowired
    private CouponKindMapper couponKindMapper;


    @Override
    public String payOrder(ClothesOrderVo clothesOrderVo) throws Exception {
        ClothesDetailsExample clothesDetailsExample = new ClothesDetailsExample();
        clothesDetailsExample.createCriteria().andClothesIdEqualTo(clothesOrderVo.getClothesId());
        List<ClothesDetailsDto> clothesDetailDtos = clothesDetailsMapper.selectByExample(clothesDetailsExample);
        if (CollectionUtils.isEmpty(clothesDetailDtos)) {
            return null;
        }
        CouponDto couponDto = new CouponDto();
        int totalFee = (int) (clothesDetailDtos.get(0).getPrice() * clothesOrderVo.getNum() * 100) ;
        if (null == clothesOrderVo.getDisId()) {
            totalFee = (int) (clothesDetailDtos.get(0).getPrice() * clothesOrderVo.getNum() * 100) ;
        } else {
            CouponExample couponExample = new CouponExample();
            couponExample.createCriteria().andDisIdEqualTo(clothesOrderVo.getDisId()).andUserIdEqualTo(clothesOrderVo.getUserId());
            List<CouponDto> couponDtos = couponMapper.selectByExample(couponExample);
            if (CollectionUtils.isEmpty(couponDtos) || couponDtos.get(0).getStatus() != 0) {
                return COUPON_INVALID;
            } else {
                CouponKindDto couponKindDto = couponKindMapper.selectByPrimaryKey(clothesOrderVo.getDisId());
                if (!clothesDetailDtos.get(0).getClothesType().equals(couponKindDto.getClothesKind())) {
                    return COUPON_TYPE_NOT_SAME;
                } else {
                    if (couponKindDto.getCouponSubPrice() > totalFee) {
                        return COUPON_MONEY_NOT_ENOUGH;
                    } else {
                        int interval = DateUtils.getDaysGapOfDates(couponDtos.get(0).getCreateTime(), new Date());
                        int status = 0;
                        if (interval > couponKindDto.getValidate()) {
                            status = 2;
                            return COUPON_DATE_OUT;
                        } else {
                            totalFee = (int) (((clothesDetailDtos.get(0).getPrice() * clothesOrderVo.getNum()* 100) - couponKindDto.getCouponPrice()*100));
                        }
                        couponDto.setStatus(status);
                        couponDto.setUpdateTime(new Timestamp(System.currentTimeMillis()));
                        couponMapper.updateByExampleSelective(couponDto, couponExample);
                    }
                }
            }
        }
        SortedMap<String, String> param = new TreeMap<>();
        param.put("appid", wxPayConfig.getAppId());
        param.put("mch_id", wxPayConfig.getMchId());
        //substring包前不包后
        param.put("nonce_str", UUIDUtils.getUUIDStrByOwnLen(32));
        param.put("body", clothesDetailDtos.get(0).getClothesName());
        param.put("out_trade_no", clothesOrderVo.getId());
        param.put("total_fee", String.valueOf(totalFee));
        param.put("spbill_create_ip", "192.168.1.239");
        param.put("notify_url", wxPayConfig.getNotifyUrl());
        param.put("trade_type", "NATIVE");
        param.put("sign", WxPayUtils.generateSignature(param, wxPayConfig.getKey()));
        String payXml = WxPayUtils.mapToXml(param);
        String result = HttpClientUtils.doPost(wxPayConfig.getUnifiedOrderUrl(), payXml, 5000);
        if (StringUtils.isNotEmpty(result)) {
            Map<String, String> resultMap = WxPayUtils.xmlToMap(result);
            if (checkResult(resultMap)) {
                if (null != clothesOrderVo.getDisId()){
                    CouponExample couponExample = new CouponExample();
                    couponExample.createCriteria().andDisIdEqualTo(clothesOrderVo.getDisId()).andUserIdEqualTo(clothesOrderVo.getUserId());
                    couponDto.setStatus(1);
                    couponDto.setUpdateTime(new Timestamp(System.currentTimeMillis()));
                    couponMapper.updateByExampleSelective(couponDto, couponExample);
                }
                return resultMap.get("code_url");
            }
            return PAY_FAIL;
        }
        return PAY_UNKNOWN_ERROR;
    }


    public boolean checkResult(Map<String, String> map) {
        return "SUCCESS".equals(map.get("return_code")) && "SUCCESS".equals(map.get("result_code"));
    }
}
