package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONObject;
import com.parada.common.param.UserParam;
import com.parada.common.service.UserService;
import com.parada.common.utils.*;
import com.parada.common.vo.UsersVo;
import com.parada.provider.dto.UsersDto;
import com.parada.provider.dto.UsersExample;
import com.parada.provider.mapper.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.parada.common.cons.SupportEntity.*;

/**
 * create by Leo 2020/07/13
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private RedisUtils redisUtils;


    @Override
    public boolean register(UserParam userParam) throws NoSuchAlgorithmException {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andPhoneNumberEqualTo(userParam.getPhoneNumber());
        long checkPhone = usersMapper.countByExample(usersExample);
        if (checkPhone > 0) {
            return false;
        } else {
            UsersDto usersDto = new UsersDto();
            usersDto.setUserId(UUIDUtils.getUUIDStrByLen());
            usersDto.setName(userParam.getName());
            usersDto.setPassword(MD5Utils.md5password(userParam.getPassword()));
            usersDto.setPhoneNumber(userParam.getPhoneNumber());
            usersMapper.insertSelective(usersDto);
            return true;
        }
    }

    @Override
    public int loginByPwd(String phoneNumber, String password, String token) throws NoSuchAlgorithmException {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andPhoneNumberEqualTo(phoneNumber);
        List<UsersDto> usersDtos = usersMapper.selectByExample(usersExample);
        if (CollectionUtils.isEmpty(usersDtos)) {
            return 1;
        } else {
            if (MD5Utils.md5password(password).equals(usersDtos.get(0).getPassword())) {
                UsersVo usersVo = new UsersVo();
                usersVo.setUserId(usersDtos.get(0).getUserId());
                usersVo.setPhoneNumber(usersDtos.get(0).getPhoneNumber());
                String userJsonStr = JSONObject.toJSONString(usersVo);
                redisUtils.set(token, userJsonStr, 20000);
                return 0;
            } else {
                return 1;
            }
        }
    }

    @Override
    public boolean checkWxUser(String openid) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andOppenIdEqualTo(openid);
        List<UsersDto> usersDtos = usersMapper.selectByExample(usersExample);
        if (CollectionUtils.isEmpty(usersDtos)) {
            return false;
        }
        return true;
    }

    @Override
    public Integer wxBindPhone(String openid, String phoneNumber, String token) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andPhoneNumberEqualTo(phoneNumber);
        List<UsersDto> usersDtos = usersMapper.selectByExample(usersExample);
        UsersDto usersDto = new UsersDto();
        if (CollectionUtils.isEmpty(usersDtos)) {
            usersDto.setUserId(UUIDUtils.getUUIDStrByLen());
            usersDto.setOppenId(openid);
            usersDto.setPhoneNumber(phoneNumber);
            UsersVo usersVo = new UsersVo();
            usersVo.setUserId(usersDto.getUserId());
            usersVo.setPhoneNumber(usersDto.getPhoneNumber());
            String userJsonStr = JSONObject.toJSONString(usersVo);
            redisUtils.set(token, userJsonStr, 20000);
            return usersMapper.insertSelective(usersDto);
        } else {
            usersDto.setOppenId(openid);
            UsersVo usersVo = new UsersVo();
            usersVo.setUserId(usersDtos.get(0).getUserId());
            usersVo.setPhoneNumber(usersDtos.get(0).getPhoneNumber());
            String userJsonStr = JSONObject.toJSONString(usersVo);
            redisUtils.set(token, userJsonStr, 20000);
            return usersMapper.updateByExampleSelective(usersDto, usersExample);
        }
    }

    @Override
    public String getPhoneNumber(String openid, String token) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andOppenIdEqualTo(openid);
        List<UsersDto> usersDtos = usersMapper.selectByExample(usersExample);
        UsersVo usersVo = new UsersVo();
        usersVo.setUserId(usersDtos.get(0).getUserId());
        usersVo.setPhoneNumber(usersDtos.get(0).getPhoneNumber());
        String userJsonStr = JSONObject.toJSONString(usersVo);
        return userJsonStr;
    }

    @Override
    public String getUser(String phoneNumber) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andPhoneNumberEqualTo(phoneNumber);
        List<UsersDto> usersDtos = usersMapper.selectByExample(usersExample);
        UsersVo usersVo = new UsersVo();
        usersVo.setUserId(usersDtos.get(0).getUserId());
        usersVo.setPhoneNumber(usersDtos.get(0).getPhoneNumber());
        String UserVoStr = JSONObject.toJSONString(usersVo);
        return UserVoStr;
    }

    @Override
    public boolean checkPhoneNumber(String phoneNumber) {
        UsersExample usersExample = new UsersExample();
        usersExample.createCriteria().andPhoneNumberEqualTo(phoneNumber);
        List<UsersDto> usersDtos = usersMapper.selectByExample(usersExample);
        if (CollectionUtils.isEmpty(usersDtos)) {
            return false;
        }
        return true;
    }
}
