package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.CollectService;
import com.parada.common.vo.CollectclothesfileVo;

import com.parada.provider.dto.ClothesDetailsDto;
import com.parada.provider.dto.ClothesDetailsExample;
import com.parada.provider.dto.CollectclothesfileDto;
import com.parada.provider.dto.CollectclothesfileExample;
import com.parada.provider.mapper.ClothesDetailsMapper;

import com.parada.provider.mapper.CollectclothesfileMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CollectServiceImpl implements CollectService {
    @Autowired
    private CollectclothesfileMapper collectclothesfileMapper;
    @Autowired
    private ClothesDetailsMapper clothesdetailsMapper;

    @Override
    public int collectClothe(String userId, String clothesId) {


        //先查询数据库中是否已经收藏此商品
        CollectclothesfileExample collectclothesfileExample = new CollectclothesfileExample();
        collectclothesfileExample.createCriteria().andUserIdEqualTo(userId).andClothesIdEqualTo(clothesId);
        List<CollectclothesfileDto> collectclothesfileDtos = collectclothesfileMapper.selectByExample(collectclothesfileExample);
        if (CollectionUtils.isEmpty(collectclothesfileDtos)) {
            CollectclothesfileDto collectclothesfileDto = new CollectclothesfileDto();
            collectclothesfileDto.setUserId(userId);
            collectclothesfileDto.setClothesId(clothesId);
            collectclothesfileDto.setCreateTime(new Date());
            return collectclothesfileMapper.insertSelective(collectclothesfileDto);

        } else {
            return 0;
        }

    }

    @Override
    public List<CollectclothesfileVo> showCollectList(String userId) {
        CollectclothesfileExample collectclothesfileExample = new CollectclothesfileExample();
        collectclothesfileExample.createCriteria().andUserIdEqualTo(userId);
        //通过用户ID查询出clothesID列表
        List<CollectclothesfileDto> collectclothesfileDtos = collectclothesfileMapper.selectByExample(collectclothesfileExample);
        List<CollectclothesfileVo> collectclothesfileVos = new ArrayList<CollectclothesfileVo>();
        collectclothesfileDtos.forEach(collectclothesfileDto -> {
            ClothesDetailsExample clothesdetailsExample = new ClothesDetailsExample();
            clothesdetailsExample.createCriteria().andClothesIdEqualTo(collectclothesfileDto.getClothesId());
            List<ClothesDetailsDto> clothesdetailsDtos = clothesdetailsMapper.selectByExample(clothesdetailsExample);
            if (!clothesdetailsDtos.isEmpty()) {
                CollectclothesfileVo collectclothesfileVo = new CollectclothesfileVo();
                BeanUtils.copyProperties(collectclothesfileDto, collectclothesfileVo);
                collectclothesfileVo.setImg(clothesdetailsDtos.get(0).getImg());
                collectclothesfileVo.setPrice(clothesdetailsDtos.get(0).getPrice());
                collectclothesfileVo.setClotheName(clothesdetailsDtos.get(0).getClothesName());
                collectclothesfileVos.add(collectclothesfileVo);
            }
        });

        return collectclothesfileVos;

    }


    @Override
    public int removeCollect(String userId, String clothesId) {
        CollectclothesfileExample collectclothesfileExample = new CollectclothesfileExample();
        collectclothesfileExample.createCriteria().andUserIdEqualTo(userId).andClothesIdEqualTo(clothesId);
        int num = collectclothesfileMapper.deleteByExample(collectclothesfileExample);
        return num;
    }
}
