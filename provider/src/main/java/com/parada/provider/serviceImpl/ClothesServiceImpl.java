package com.parada.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.parada.common.service.ClothesService;
import com.parada.common.utils.RedisUtils;
import com.parada.provider.dto.ClothesDetailsDto;
import com.parada.provider.dto.ClothesDetailsExample;
import com.parada.provider.dto.ClothesOrderDto;
import com.parada.provider.dto.ClothesOrderExample;
import com.parada.provider.mapper.ClothesDetailsMapper;
import com.parada.provider.mapper.ClothesOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;

import static com.parada.common.cons.SupportEntity.*;

/**
 * create by Leo 2020/07/20
 */
@Service
public class ClothesServiceImpl implements ClothesService {
    @Autowired
    private ClothesDetailsMapper clothesDetailsMapper;

    @Autowired
    private ClothesOrderMapper clothesOrderMapper;


    @Override
    public void addSale(String orderId) {
        ClothesOrderDto clothesOrderDto = clothesOrderMapper.selectByPrimaryKey(orderId);
        ClothesDetailsExample clothesDetailsExample = new ClothesDetailsExample();
        clothesDetailsExample.createCriteria().andClothesIdEqualTo(clothesOrderDto.getClothesId());
        Integer num = clothesDetailsMapper.selectByExample(clothesDetailsExample).get(0).getSaled();
        ClothesDetailsDto clothesDetailsDto = new ClothesDetailsDto();
        clothesDetailsDto.setSaled(num+clothesOrderDto.getNumber());
        clothesDetailsDto.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        clothesDetailsMapper.updateByExampleSelective(clothesDetailsDto, clothesDetailsExample);

    }
}
