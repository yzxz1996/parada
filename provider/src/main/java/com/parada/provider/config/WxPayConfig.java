package com.parada.provider.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * create by Leo 2020/07/10
 */
@Data
@Component
@ConfigurationProperties(prefix = "wxPay")
public class WxPayConfig {
    private String appId;
    private String mchId;
    private String key;
    private String unifiedOrderUrl;
    private String notifyUrl;
}
