package com.parada.provider.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by boot on 2020/6/16.
 */
@Configuration
@MapperScan(basePackages = "com.parada.provider.mapper")
public class MybatisConfig {
}
