package com.parada.common.param;

import lombok.Data;

/**
 * create by Leo 2020/07/16
 */
@Data
public class ClothesOrderParam {

    private String clothesId;

    private Integer num;

    private Integer disId;


}
