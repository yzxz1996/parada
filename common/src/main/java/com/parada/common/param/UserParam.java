package com.parada.common.param;

import lombok.Data;

import java.io.Serializable;

/**
 * create by Leo 2020/07/13
 */
@Data
public class UserParam implements Serializable {
    private String phoneNumber;
    private String name;
    private String code;
    private String password;
}
