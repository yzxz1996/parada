package com.parada.common.param;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UsersInfoParam implements Serializable {

    private String name;

    private String password;

    private String address;

    private String newPhoneNumber;

    private Integer sex;

    private Integer age;

    private String img;

    private String pCA;

    private String country;


}