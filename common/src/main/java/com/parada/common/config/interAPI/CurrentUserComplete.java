package com.parada.common.config.interAPI;

import com.parada.common.utils.RedisUtils;
import com.parada.common.vo.UsersVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

/**
 * create by Leo 2020/07/14
 */
public class CurrentUserComplete implements HandlerMethodArgumentResolver {


    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.hasParameterAnnotation(CurrentUser.class)
                && methodParameter.getParameterType().isAssignableFrom(UsersVo.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        UsersVo usersVo = (UsersVo) nativeWebRequest.getAttribute("usersVo", SCOPE_REQUEST);
        if (!ObjectUtils.isEmpty(usersVo)) {
            return usersVo;
        }
        return null;
    }
}
