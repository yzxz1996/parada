package com.parada.common.config.interAPI;

import com.alibaba.fastjson.JSONObject;
import com.parada.common.utils.RedisUtils;
import com.parada.common.vo.UsersVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * create by Leo 2020/07/02
 */
public class LoginRequiredComplete implements HandlerInterceptor {
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        LoginRequired annotation = method.getAnnotation(LoginRequired.class);
        if (null != annotation) {
            String token = httpServletRequest.getHeader("token");
            if (StringUtils.isEmpty(token)) {
                throw new RuntimeException("login error");
            }
            String redisToken = (String) redisUtils.get(token);
            //key过期了
            if (StringUtils.isEmpty(redisToken)) {
                throw new RuntimeException("please login");
            }
            UsersVo usersVo = JSONObject.parseObject(redisToken, UsersVo.class);
            httpServletRequest.setAttribute("usersVo", usersVo);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
