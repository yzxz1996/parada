package com.parada.common.service;

import com.parada.common.vo.BannerVo;

import java.util.List;

public interface BannerService {
    List<BannerVo> queryBanner();
}
