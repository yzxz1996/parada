package com.parada.common.service;

import com.parada.common.vo.HotwordVo;
import org.apache.catalina.LifecycleState;

import java.util.List;

/**
 * Created by yuhw on 2020/7/15
 */
public interface HotWordService {

    //向数据库里添加热词
    void addHotWord(String clothesName);

    //根据热词的次数展示排行前十位
    List<HotwordVo> showHotWord();
}
