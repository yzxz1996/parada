package com.parada.common.service;

import com.parada.common.vo.CollectclothesfileVo;

import java.util.List;

public interface CollectService {
    int collectClothe(String userId, String clothesId);

    List<CollectclothesfileVo> showCollectList(String userId);

    int removeCollect(String userId, String clothesId);
}
