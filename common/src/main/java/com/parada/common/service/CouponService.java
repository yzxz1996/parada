package com.parada.common.service;

import com.parada.common.vo.CouponVo;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * create by CuiGeGe 2020/07/16
 */
public interface CouponService {
    boolean couponReceive(String userId, Integer disId);

    List<CouponVo> showCoupon(String userId);
}
