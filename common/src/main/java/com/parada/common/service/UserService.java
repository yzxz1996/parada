package com.parada.common.service;

import com.parada.common.param.UserParam;
import com.parada.common.utils.ReturnResult;

import java.security.NoSuchAlgorithmException;

/**
 * create by Leo 2020/07/13
 */
public interface UserService {

    boolean register(UserParam userParam) throws NoSuchAlgorithmException;

    int loginByPwd(String phoneNumber, String password, String token) throws NoSuchAlgorithmException;

    boolean checkWxUser(String openid);

    Integer wxBindPhone(String openid, String phoneNumber, String token);

    String getPhoneNumber(String openid, String token);

    String getUser(String phoneNumber);

    boolean checkPhoneNumber(String phoneNumber);
}
