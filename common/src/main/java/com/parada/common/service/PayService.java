package com.parada.common.service;

import com.parada.common.vo.ClothesOrderVo;

/**
 * create by Leo 2020/07/16
 */
public interface PayService {

    String payOrder(ClothesOrderVo clothesOrderVo) throws Exception;

}
