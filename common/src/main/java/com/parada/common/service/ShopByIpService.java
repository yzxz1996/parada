package com.parada.common.service;

import com.parada.common.utils.ReturnResult;
import com.parada.common.vo.ShopcarVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by yuhw on 2020/7/13
 */
public interface ShopByIpService {
    //String getIp(HttpServletRequest request);
    //查看当前ip下有没有购物车，如果有，删除此购物车
    boolean delCarByIp(String ip);

    //根据ip添加购物车
    int addCarByIp(String ip, String clothesId, int count);

    //根据ip查看购物车
    List<ShopcarVo> lookCarByIp(String ip);

    //根据uesrId查看购物车
    List<ShopcarVo> lookCarByUserId(String userId);
}
