package com.parada.common.service;

import com.parada.common.vo.ConsultVo;

import java.util.List;

/**
 * create by CuiGeGe 2020/07/17
 */
public interface ConsultService {
    List<ConsultVo> userConsult(String userId, String details);
}
