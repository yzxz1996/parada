package com.parada.common.service;

import com.parada.common.param.UsersInfoParam;
import com.parada.common.vo.UserInfoVo;
import com.parada.common.vo.UsersVo;

/**
 * create by Leo 2020/07/14
 */
public interface UserInfoService {

    UserInfoVo showUser(String userId);

    Integer modifyUserInfo(UsersVo usersVo, UsersInfoParam usersInfoParam);
}
