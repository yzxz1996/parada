package com.parada.common.service;

import com.parada.common.utils.ReturnResult;
import com.parada.common.vo.ScoresVo;

import java.util.List;

/**
 * Created by yuhw on 2020/7/15
 */
public interface ScoresService {

    //评价
    boolean addScores(String userId, String clothesId, String assess, int scores);

    //查看评价
    List<ScoresVo> showScores(String clothesId);

}
