package com.parada.common.service;

import com.parada.common.vo.ClothesDetailsVo;
import com.parada.common.vo.ClothesDetailsVoMsg;
import com.parada.common.vo.ClothesKindVo;
import com.parada.common.vo.ShopcarVo;

import java.text.ParseException;
import java.util.List;

/**
 * Created by yuhw on 2020/7/13
 */
public interface ShowClothesService {

    //根据名字模糊查询（可以在分类下查）
    List<ClothesDetailsVo> showClothesByName(int pageNo, String clothesName, String clothesType) throws ParseException;

    //根据种类展示所有分类下的商品列表
    List<ClothesDetailsVo> showClothesByType(int pageNo, String clothesType) throws ParseException;

    //根据性别分类展示分类下所有商品种类
    List<ClothesKindVo> showClothesBySex(int sexId);

    //根据商品id展示商品详情
    ClothesDetailsVoMsg showClothesMsg(String clothesId);

    //根据用户id来添加购物车，首先得是登录状态
    int addCarByUserId(String userId, String ip, String clothesId, int count);

    //根据userId查看购物车
    List<ShopcarVo> lookCarByUesrId(String userId);
}
