package com.parada.common.service;

import com.parada.common.vo.ClothesOrderVo;

/**
 * create by Leo 2020/07/16
 */
public interface ClothesOrderService {

    boolean checkNum(ClothesOrderVo clothesOrderVo);

    void insertOrder(ClothesOrderVo clothesOrderVo);

    void cutCountAndUpdateOrders(String orderId);
}
