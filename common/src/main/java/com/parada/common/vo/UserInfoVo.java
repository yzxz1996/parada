package com.parada.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserInfoVo implements Serializable {


    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "电话号码")
    private String phoneNumber;

    @ApiModelProperty(value = "微信id")
    private String oppenId;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "头像")
    private String img;

    @ApiModelProperty(value = "省市区")
    private String pCA;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;


}