package com.parada.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class ClothesOrderVo implements Serializable {

    @ApiModelProperty(value = "订单id")
    private String id;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "商品id")
    private String clothesId;

    @ApiModelProperty(value = "数量")
    private Integer num;

    @ApiModelProperty(value = "优惠券id")
    private Integer disId;


}