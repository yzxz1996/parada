package com.parada.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by yuhw on 2020/7/14
 */
@Data
public class IpaddressVo {

    @ApiModelProperty(value = "ip地址")
    private String ipaddress;
}
