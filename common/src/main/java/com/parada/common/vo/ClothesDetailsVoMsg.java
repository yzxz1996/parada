package com.parada.common.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

import java.io.Serializable;
import java.util.Date;

/**
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table clothesdetails
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class ClothesDetailsVoMsg implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.clothes_id
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品id")
    private String clothesId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.clothes_type
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品分类")
    private String clothesType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.clothes_name
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品名")
    private String clothesName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.clothes_color
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品颜色")
    private String clothesColor;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.clothes_size
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品尺寸")
    private String clothesSize;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.stock
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品库存")
    private Integer stock;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.img
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品图片链接")
    private String img;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.price
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品单价")
    private Double price;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.detail
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品描述")
    private String detail;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.saled
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "已售件数")
    private Integer saled;


    @ApiModelProperty(value = "商品的平均评分")
    private String scores;

    public void setScores(String scores) {
        this.scores = scores;
    }

    public String getScores() {
        return scores;
    }

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.create_time
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品上架时间")
    private String createTimee;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column clothesdetails.update_time
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "商品更新时间")
    private String updateTimee;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.clothes_id
     *
     * @return the value of clothesdetails.clothes_id
     * @mbg.generated
     */
    public String getClothesId() {
        return clothesId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.clothes_id
     *
     * @param clothesId the value for clothesdetails.clothes_id
     * @mbg.generated
     */
    public void setClothesId(String clothesId) {
        this.clothesId = clothesId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.clothes_type
     *
     * @return the value of clothesdetails.clothes_type
     * @mbg.generated
     */
    public String getClothesType() {
        return clothesType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.clothes_type
     *
     * @param clothesType the value for clothesdetails.clothes_type
     * @mbg.generated
     */
    public void setClothesType(String clothesType) {
        this.clothesType = clothesType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.clothes_name
     *
     * @return the value of clothesdetails.clothes_name
     * @mbg.generated
     */
    public String getClothesName() {
        return clothesName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.clothes_name
     *
     * @param clothesName the value for clothesdetails.clothes_name
     * @mbg.generated
     */
    public void setClothesName(String clothesName) {
        this.clothesName = clothesName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.clothes_color
     *
     * @return the value of clothesdetails.clothes_color
     * @mbg.generated
     */
    public String getClothesColor() {
        return clothesColor;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.clothes_color
     *
     * @param clothesColor the value for clothesdetails.clothes_color
     * @mbg.generated
     */
    public void setClothesColor(String clothesColor) {
        this.clothesColor = clothesColor;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.clothes_size
     *
     * @return the value of clothesdetails.clothes_size
     * @mbg.generated
     */
    public String getClothesSize() {
        return clothesSize;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.clothes_size
     *
     * @param clothesSize the value for clothesdetails.clothes_size
     * @mbg.generated
     */
    public void setClothesSize(String clothesSize) {
        this.clothesSize = clothesSize;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.stock
     *
     * @return the value of clothesdetails.stock
     * @mbg.generated
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.stock
     *
     * @param stock the value for clothesdetails.stock
     * @mbg.generated
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.img
     *
     * @return the value of clothesdetails.img
     * @mbg.generated
     */
    public String getImg() {
        return img;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.img
     *
     * @param img the value for clothesdetails.img
     * @mbg.generated
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.price
     *
     * @return the value of clothesdetails.price
     * @mbg.generated
     */
    public Double getPrice() {
        return price;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.price
     *
     * @param price the value for clothesdetails.price
     * @mbg.generated
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.detail
     *
     * @return the value of clothesdetails.detail
     * @mbg.generated
     */
    public String getDetail() {
        return detail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.detail
     *
     * @param detail the value for clothesdetails.detail
     * @mbg.generated
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.saled
     *
     * @return the value of clothesdetails.saled
     * @mbg.generated
     */
    public Integer getSaled() {
        return saled;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.saled
     *
     * @param saled the value for clothesdetails.saled
     * @mbg.generated
     */
    public void setSaled(Integer saled) {
        this.saled = saled;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.create_time
     *
     * @return the value of clothesdetails.create_time
     * @mbg.generated
     */
    public String getCreateTimee() {
        return createTimee;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.create_time
     *
     * @param createTimee the value for clothesdetails.create_time
     * @mbg.generated
     */
    public void setCreateTimee(String createTimee) {
        this.createTimee = createTimee;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column clothesdetails.update_time
     *
     * @return the value of clothesdetails.update_time
     * @mbg.generated
     */
    public String getUpdateTimee() {
        return updateTimee;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column clothesdetails.update_time
     *
     * @param updateTimee the value for clothesdetails.update_time
     * @mbg.generated
     */
    public void setUpdateTimee(String updateTimee) {
        this.updateTimee = updateTimee;
    }
}