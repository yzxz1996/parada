package com.parada.common.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table banner
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class BannerVo implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column banner.id
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "banner标识符")
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column banner.img
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "图片")
    private String img;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column banner.explainWord
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "详细内容")
    private String explainword;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column banner.link
     *
     * @mbg.generated
     */
    @ApiModelProperty(value = "链接")
    private String link;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column banner.id
     *
     * @return the value of banner.id
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column banner.id
     *
     * @param id the value for banner.id
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column banner.img
     *
     * @return the value of banner.img
     * @mbg.generated
     */
    public String getImg() {
        return img;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column banner.img
     *
     * @param img the value for banner.img
     * @mbg.generated
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column banner.explainWord
     *
     * @return the value of banner.explainWord
     * @mbg.generated
     */
    public String getExplainword() {
        return explainword;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column banner.explainWord
     *
     * @param explainword the value for banner.explainWord
     * @mbg.generated
     */
    public void setExplainword(String explainword) {
        this.explainword = explainword;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column banner.link
     *
     * @return the value of banner.link
     * @mbg.generated
     */
    public String getLink() {
        return link;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column banner.link
     *
     * @param link the value for banner.link
     * @mbg.generated
     */
    public void setLink(String link) {
        this.link = link;
    }
}