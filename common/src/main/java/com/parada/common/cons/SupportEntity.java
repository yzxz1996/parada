package com.parada.common.cons;

/**
 * create by Leo 2020/07/13
 */
public class SupportEntity {
    //操作成功的状态码
    public final static Integer SUCCESS_STATUS_CODE = 200;

    //操作失败的状态码
    public final static Integer FAIL_STATUS_CODE = 499;

    //登录或注册成功
    public final static String LOGIN_REGISTER_SUCCESS = "成功";

    //重复注册
    public final static String LOGIN_AGAIN = "重复注册";

    //注册验证码命名空间
    public final static String REGISTER_PHONE_SMSCODE = "register_phone_smscode:";

    //登录验证码命名空间
    public final static String LOGIN_PHONE_SMSCODE = "login_phone_smscode:";

    //用户不存在或密码不对
    public final static String USER_NOT_EXIST_OR_PWD_ERROR = "用户不存在或密码不对!";

    //登录成功
    public final static String USER_LOGIN_SUCCESS = "登录成功!";

    //发送验证码成功
    public final static String SEND_CODE_SUCCESS = "发送验证码成功!";

    //发送验证码失败
    public final static String SEND_CODE_FAIL = "发送验证码失败!";

    //号码不对或验证码已过期
    public final static String ERROR_PHONENUMBER_CODE = "号码不对或验证码已过期!";

    //验证码不对
    public final static String ERROR_CODE = "验证码不对!";

    //请绑定手机号
    public final static String BIND_WX_PHONE = "请绑定手机号!";

    //微信绑定手机号成功
    public final static String BIND_WX_PHONE_SUCCESS = "微信绑定手机号成功";

    //微信绑定手机号失败
    public final static String BIND_WX_PHONE_FAIL = "微信绑定手机号失败";

    //退出登录成功
    public final static String QUIT_LOGIN_SUCCESS = "退出登录成功";

    //退出登录失败
    public final static String QUIT_LOGIN_FAIL = "退出登录失败";

    //个人信息展示
    public final static String USERINFO_EXHIBITION = "个人信息展示";

    //跳转登录页面
    public final static String REDIRECT_LOGIN = "跳转登录页面";

    //修改个人信息展示成功
    public final static String MODIFY_USERINFO_SUCCESS = "修改个人信息展示成功";

    //修改个人信息展示失败
    public final static String MODIFY_USERINFO_FAIL = "修改个人信息展示失败";

    //查无此人
    public final static String USER_NOT_EXIST = "修改个人信息展示失败";

    //未知错误
    public final static String UNKNOWN_ERROR = "未知错误";

    //消息中间件
    public final static String SEND_QUEUE_ORDER_NAME = "send_queue_order_name";

    //支付未知错误
    public final static String PAY_UNKNOWN_ERROR = "支付未知错误";

    //支付失败
    public final static String PAY_FAIL = "支付失败";

    //支付回调
    public final static String WX_PAY_NOTIFY_NAMESPACE = "wx_pay_notify_namespace:";

    //查无此优惠券或优惠券已使用or已过期
    public final static String COUPON_INVALID = "查无此优惠券或优惠券已使用or已过期:";

    //未达到优惠券满减金额
    public final static String COUPON_MONEY_NOT_ENOUGH = "未达到优惠券满减金额:";

    //优惠券已过期
    public final static String COUPON_DATE_OUT = "优惠券已过期:";

    //库存上锁
    public final static String GOODS_COUNT_NAMESPACE_LOCK = "goods_count_namespace_lock:";

    //优惠券可优惠的种类不一样
    public final static String COUPON_TYPE_NOT_SAME = "优惠券可优惠的种类不一样:";

    //商品卖出数量
    public final static String CLOTHES_SALE_SUCCESS = "商品卖出数量:";

    //查看点赞频率
    public final static String CHECK_REQ_NAMESPACE = "check_like_freq:";

    //点赞历史表
    public final static String LIKE_HISTORY_LIST = "like_history_list:";

    //是否点赞
    public final static String GOODS_LIKE_SUM = "goods_like_sum:";

    //3天
    public final static String DAY_THREE = "day_three:";

    //3月
    public final static String MONTH_THREE = "month_three:";

    //3年
    public final static String YEAR_THREE = "year_three:";

}
