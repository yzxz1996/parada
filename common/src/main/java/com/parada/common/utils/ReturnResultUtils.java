package com.parada.common.utils;

/**
 * Created by boot on 2020/6/17.
 */
public class ReturnResultUtils {
    /**
     * 返回不带data的成功
     *
     * @return
     */
    public static ReturnResult returnSuccess(int code, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        return returnResult;
    }

    public static <T> ReturnResult returnSuccess(int code, String msg, T data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        returnResult.setData(data);
        return returnResult;
    }

    public static ReturnResult returnFail(int code, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        return returnResult;
    }

    public static ReturnResult returnCodeAndMsg(int code, String msg) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMsg(msg);
        return returnResult;
    }
}
