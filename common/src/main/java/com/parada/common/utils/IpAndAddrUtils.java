package com.parada.common.utils;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.lang.model.util.Elements;
import javax.servlet.http.HttpServletRequest;
import java.net.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;

/**
 * Created by yuhw on 2020/7/13
 */
@Component
public class IpAndAddrUtils {
    /**
     * 获取发起请求的IP地址
     */
    public static String getIp(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
                //根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                ipAddress = inet.getHostAddress();
            }
        }
        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ipAddress != null && ipAddress.length() > 15) { //"***.***.***.***".length() = 15
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }
        return ipAddress;

    }


//    private static String getInternetIp() throws Exception{
//        try {
//            // 打开连接
//            Document doc = Jsoup.connect("http://chaipip.com/").get();
//            Elements eles = doc.select("#ip");
//            return eles.attr("value");
//        }catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return InetAddress.getLocalHost().getHostAddress();
//    }


    public String getV4IP() {
        String ip = "";
        String chinaz = "http://ip.chinaz.com";

        StringBuilder inputLine = new StringBuilder();
        String read = "";
        URL url = null;
        HttpURLConnection urlConnection = null;
        BufferedReader in = null;
        try {
            url = new URL(chinaz);
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            while ((read = in.readLine()) != null) {
                inputLine.append(read + "\r\n");
            }
            //System.out.println(inputLine.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }


        Pattern p = Pattern.compile("\\<dd class\\=\"fz24\">(.*?)\\<\\/dd>");
        Matcher m = p.matcher(inputLine.toString());
        if (m.find()) {
            String ipstr = m.group(1);
            ip = ipstr;
            //System.out.println(ipstr);
        }

        return ip;
    }

    public String getIpAndDistance() {
        String[] cmds = {"curl", "icanhazip.com"};
        return execCurl(cmds).trim();
    }

    public String execCurl(String[] cmds) {
        ProcessBuilder process = new ProcessBuilder(cmds);
        Process p;
        try {
            p = process.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(System.getProperty("line.separator"));
            }
            return builder.toString();

        } catch (IOException e) {
            System.out.print("error");
            e.printStackTrace();
        }
        return null;
    }


    public static String decodeUnicode(String theString) {
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len; ) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException("Malformed encoding.");
                        }
                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't') {
                        aChar = '\t';
                    } else if (aChar == 'r') {
                        aChar = '\r';
                    } else if (aChar == 'n') {
                        aChar = '\n';
                    } else if (aChar == 'f') {
                        aChar = '\f';
                    }
                    outBuffer.append(aChar);
                }
            } else {
                outBuffer.append(aChar);
            }
        }
        return outBuffer.toString();
    }

    public static String getAddressResult(String ip) {
        // 创建默认http连接
        HttpClient client = HttpClients.createDefault();
        // 创建一个post请求
        HttpPost post = new HttpPost("http://api.map.baidu.com/location/ip");
        List<NameValuePair> paramList = new ArrayList<NameValuePair>();
        // 传递的参数
        paramList.add(new BasicNameValuePair("ip", ip));
        paramList.add(new BasicNameValuePair("ak", "KUfSS2wiOMVzQENWGTdS9o7P4Yj7Kd31"));
        paramList.add(new BasicNameValuePair("sn", ""));
        paramList.add(new BasicNameValuePair("coor", ""));

        String address = "";
        try {
            // 把参转码后放入请求实体中
            HttpEntity entitya = new UrlEncodedFormEntity(paramList, "utf-8");
            post.setEntity(entitya);// 把请求实体放post请求中
            // 用http连接去执行get请求并且获得http响应
            HttpResponse response = client.execute(post);
            // 从response中取到响应实体
            HttpEntity entity = response.getEntity();
            // 把响应实体转成文本
            String str = EntityUtils.toString(entity);

            // 分隔解析
            if (str.length() > 0) {
                int index = str.indexOf("province");
                int index2 = str.indexOf("city");
                int index3 = str.indexOf("district");
                String province = str.substring(index + 11, index2 - 3);
                String city = str.substring(index2 + 7, index3 - 3);
                // System.out.println(province);
                // System.out.println(city);
                address = decodeUnicode(province) + "," + decodeUnicode(city);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return address;
    }

    // 测试
//    public static void main(String[] args) throws IOException {
//        IpAddressUtil ipAddressUtil = new IpAddressUtil();
//        String ip = "59.42.239.26";
//        System.out.println(ipAddressUtil.getAddressResult(ip));
//    }

}

//    /**
//     * 获取发起请求的浏览器名称
//     */
//    public static String getBrowserName(HttpServletRequest request) {
//        String header = request.getHeader("User-Agent");
//        UserAgent userAgent = UserAgent.parseUserAgentString(header);
//        Browser browser = userAgent.getBrowser();
//        return browser.getName();
//    }

//    /**
//     * 获取发起请求的浏览器版本号
//     */
//    public static String getBrowserVersion(HttpServletRequest request) {
//        String header = request.getHeader("User-Agent");
//        UserAgent userAgent = UserAgent.parseUserAgentString(header);
//        //获取浏览器信息
//        Browser browser = userAgent.getBrowser();
//        //获取浏览器版本号
//        Version version = browser.getVersion(header);
//        return version.getVersion();
//    }

//    /**
//     * 获取发起请求的操作系统名称
//     */
//    public static String getOsName(HttpServletRequest request) {
//        String header = request.getHeader("User-Agent");
//        UserAgent userAgent = UserAgent.parseUserAgentString(header);
//        OperatingSystem operatingSystem = userAgent.getOperatingSystem();
//        return operatingSystem.getName();
//    }

