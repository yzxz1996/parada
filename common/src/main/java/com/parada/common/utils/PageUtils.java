package com.parada.common.utils;

import lombok.Data;

import java.util.Collection;

/**
 * Created by yuhw on 2020/6/17
 */
@Data
public class PageUtils<T> {
    private int currentPage;//当前页
    private int pageNo;//页码
    private int pageSize = 3;//每页条数
    private long totalPage;//总页数
    private long totalCount;//总条数
    private Collection<T> currentList;//查询的所有内容

    public int getPageNo() {
        return (pageNo - 1) * pageSize;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public long getTotalPage() {
        return totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }


}
