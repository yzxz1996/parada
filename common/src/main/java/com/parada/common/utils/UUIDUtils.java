package com.parada.common.utils;

import java.util.UUID;

/**
 * Created by xueqijun on 2020/7/9.
 */
public class UUIDUtils {
    public static String getUUIDStr() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String getUUIDStrByLen() {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
    }

    public static String getUUIDStrByOwnLen(Integer len) {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, len);
    }
}
